﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	//Movement 
	float movementSpeed = 10f ;
	float maxSpeed = 6f;	
	float jumpPower = 250f;
	public bool grounded;
	public float health;
	public bool invincible;

	//General
	private Rigidbody2D rb2d;
	private Animator anim;

	// Use this for initialization
	void Start () {
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		anim = gameObject.GetComponent<Animator> ();
		health = 3;
		invincible = false;
	}

	void Update () {

		//Death
		if (health == 0) {
			Destroy(gameObject);
		}



		//Actions
		anim.SetBool ("Grounded", grounded);
		anim.SetFloat ("Speed", Mathf.Abs (Input.GetAxis ("Horizontal")));

		if (Input.GetKey ("h")) {
			anim.Play("Attack");
		}

		if (Input.GetAxis("Horizontal") < -0.1f) {
			transform.localScale = new Vector3(1,1,1);
		}

		if (Input.GetAxis("Horizontal") > 0.1f) {
			transform.localScale = new Vector3(-1,1,1);
		}

		if (Input.GetButtonDown ("Jump") && grounded){
			rb2d.AddForce (Vector2.up * jumpPower);
		}

		if (Input.GetAxis ("Horizontal") == 0) {
			rb2d.velocity = (new Vector2(0,rb2d.velocity.y));
			return;
		}
	}



	// Update is called once per frame
	void FixedUpdate () {



		float h = Input.GetAxis ("Horizontal");

		rb2d.AddForce ((Vector2.right * movementSpeed) * h);


	

		if (rb2d.velocity.x > maxSpeed) {
			rb2d.velocity = new Vector2(maxSpeed , rb2d.velocity.y);
		}

		if (rb2d.velocity.x < -maxSpeed) {
			rb2d.velocity = new Vector2(-maxSpeed , rb2d.velocity.y);
		}



		}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Enemy" || col.tag == "Projectile") {
			if (invincible == false){
				invincible = true;
				health -= 1;
				Debug.Log ("Commence à wait");
				StartCoroutine(Wait());
				Debug.Log("Fini de wait");
				invincible = false;

			}
		}

	}

	IEnumerator Wait() {
		Debug.Log ("JSUIS INVINCIBLE");
		yield return new WaitForSeconds(3);

		Debug.Log ("Plus maintenant");
	}
}