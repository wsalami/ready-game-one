﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {
	
	private Transform target;
	private float trackSpeed = 10;
	public GameObject t;
	
	// Set target
	void Start() {
		target = t.GetComponent<Transform>();
	}
	
	// Track target
	void LateUpdate() {
		if (target) {
			var v = transform.position;
			v.x = target.position.x;
			transform.position = Vector3.MoveTowards (transform.position, v, trackSpeed * Time.deltaTime);
		}
	}
}
