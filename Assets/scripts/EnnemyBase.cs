﻿using UnityEngine;
using System.Collections;

public class EnnemyBase : MonoBehaviour {

	public float health ;

	public Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
		health = 3;
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (health == 0)
			Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			col.GetComponent<Player>().health -= 1;
		}
	}
}
