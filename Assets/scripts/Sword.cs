﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Enemy") {
			col.GetComponent<EnnemyBase>().health -= 1;
		}
	}
	
}
